// Package twofer should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package twofer

// Function to share something with you or with ....
func ShareWith(name string) string {

	var who string

	if len(name) == 0 {
		who = "you"
	} else {
		who = name
	}

	return "One for " + who + ", one for me."
}
