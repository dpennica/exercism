<?php

/**
 * Bob
 */
class Bob
{

    private $responses = [
        'question' => 'Sure.',
        'yell' => 'Whoa, chill out!',
        'addressed' => 'Fine. Be that way!',
        'default' => 'Whatever.'
    ];

    public function respondTo(string $var = null)
    {
        mb_internal_encoding('UTF8');
        $return = $this->responses['default'];
        $var = trim($var, " \t\n\r\0\x0B\u000b\u00a0\u2002");

        if (empty($var)) {
           $return = $this->responses['addressed'];
        }
        elseif ( mb_strtoupper($var) === $var && mb_strtolower($var) !== $var)
        {
          $return = $this->responses["yell"];
        }
        elseif (preg_match("/\?$/", $var) ) {
            $return = $this->responses['question'];
        }

        return $return;
    }
}