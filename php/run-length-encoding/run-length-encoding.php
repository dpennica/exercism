<?php

//
// This is only a SKELETON file for the "Run Length Encoding" exercise. It's been provided as a
// convenience to get you started writing code faster.
//

function encode($input)
{
    $pattern = '/(.)\1*/';

    $result = preg_replace_callback($pattern,
        function ($str) {
            $strlen = strlen($str[0]);
            return ($strlen >1 ? $strlen : ''). $str[1];
        },
        $input);
    return $result;

}

function decode($input)
{
    $pattern = '/(\d+)(\D)/';

    $result = preg_replace_callback($pattern,
        function ($str) {
            return str_repeat($str[2],$str[1]);
        },
        $input);
    return $result;
}