<?php


$romans = [
  1000 => 'M',
  900 => 'CM',
  500 => 'D',
  400 => 'CD',
  100 => 'C',
  90 => 'XC',
  50 => 'L',
  40 => 'XL',
  10 => 'X',
  9 => 'IX',
  5 => 'V',
  4 => 'IV',
  1 => 'I'
];



function toRoman(int $num) {
  if ($num <= 0) {
    return 0;
  }

  global $romans;

  $roman = '';
  foreach ($romans as $d => $r)
  {
    while ($num >= $d)
    {
      $roman .= $r;
      $num -= $d;
    }
  }
  return $roman;
}
