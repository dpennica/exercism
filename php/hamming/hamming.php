<?php



// function distance($a, $b)
// {
//   $differences = 0;
//   $aLen = strlen($a);
//
//   if ($aLen !== strlen($b))
//   {
//     throw new InvalidArgumentException("Must be equal", 1);
//   }
//
//   if ($a === $b)
//   {
//     return $differences;
//   }
//   for($i=0; $i < $aLen; $i++) {
//     if ( $a[$i] !== $b[$i] ){
//       $differences++;
//     }
//   }
//   return $differences;
// }

function distance($a, $b)
{
  if ($a === $b) return 0;
  if (strlen($a) !== strlen($b))
  {
    throw new InvalidArgumentException("Must be equal", 1);
  }
  return calculateDifferences($a,$b);
}

function calculateDifferences($a,$b)
{
  $differences = 0;
  $aLen = strlen($a);

  for($i=0; $i < $aLen; $i++) {
    if ( $a[$i] !== $b[$i] ){
      $differences++;
    }
  }
  return $differences;
}

echo distance('A', 'B');