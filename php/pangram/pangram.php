<?php
/**
 * User: Davide Pennica
 * Email: <pennicad@gmail.com>
 * Date: 10/12/17
 * Time: 16.07
 */

function isPangram($string)
{
    if ( ! strlen($string) ){
        return false;
    }

    $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $str_split1 = str_split(strtoupper($string));
    $str_split2 = str_split($alphabet);

    return count(array_diff($str_split2, $str_split1)) === 0;
}