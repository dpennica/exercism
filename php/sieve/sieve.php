<?php

function sieve(int $limit) {
  if ( 1 >= $limit) {
    return [];
  }

  $numbers = array_fill_keys(range(2, $limit), true);

  for ($i=2; $i <= sqrt($limit); $i++) {
    if(isset($numbers[$i]) && $numbers[$i]) {
      for ($n=pow($i, 2); $n <= $limit;  $n += $i) {
        unset ($numbers[$n]) ;
      }
    }
  }

  return array_keys($numbers);

}