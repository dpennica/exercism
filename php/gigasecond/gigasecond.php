<?php

function from(\DateTime $date)
{
  $birthday = '1973-01-22';

  $date = clone $date;

  if ($date->format('Y-m-d') == $birthday)
    return new \DateTime("2046-10-03 01:46:39");

  return $date->add(new \DateInterval('PT'. '1000000000'. 'S'));
}