<?php

function toRna(string $dna) {
  $dnaToRna = [
      'G' => 'C',
      'C' => 'G',
      'T' => 'A',
      'A' => 'U'
  ];

  $rna = '';
  $dna = str_split($dna);
  foreach ( $dna as $d )
  {
    $rna .= $dnaToRna[$d];
  }
  return $rna;
}
